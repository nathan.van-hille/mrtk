﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Microsoft.MixedReality.Toolkit.UI;
using Microsoft.MixedReality.Toolkit.Utilities;

public class FeatureSelectionManager : MonoBehaviour
{

    public DataRepository data;
    public GameObject buttonPrefab;
    public GameObject buttonParent;

    public ToggleButton toggle;

    private string activeButtonBuffer;

    public void openMenu()
    {
        Clear();
        Populate();
    }

    public void Clear()
    {
        foreach (Transform child in buttonParent.transform)
        {
            child.gameObject.SetActive(false);
            GameObject.Destroy(child.gameObject);
        }
        toggle.buttons.Clear();
    }
    public void Populate()
    {
        var grid = buttonParent.GetComponent<GridObjectCollection>();
        grid.UpdateCollection();
        var features = data.columnList;
        foreach (string feature in features)
        {
            if(feature != "id" && data.featureList[feature].Count > 0)
            {
                GameObject button = Instantiate(buttonPrefab,
                new Vector3(0, 0, 0),
                Quaternion.identity);

                button.transform.SetParent(buttonParent.transform, false);
                button.transform.name = "FeatureButton_" + feature;

                var buttonConfig = button.GetComponent<ButtonConfigHelper>();
                buttonConfig.MainLabelText = feature;
                //toggle += trajectory.toggleDisplay;
                buttonConfig.OnClick.AddListener(()=>toggle.toggleButton(button.gameObject));
                toggle.buttons.Add(button.GetComponent<PressableButtonHoloLens2>());
            }
            
        }
        
        
        grid.UpdateCollection();
    }

    public string selectedFeature()
    {
        if (toggle.activeButton != null)
        {
            return toggle.activeButton.gameObject.GetComponent<ButtonConfigHelper>().MainLabelText;
        }
        else
        {
            return "none";
        }
    }
}
