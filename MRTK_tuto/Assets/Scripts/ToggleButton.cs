﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Microsoft.MixedReality.Toolkit.UI;

public class ToggleButton : MonoBehaviour
{
    public PressableButtonHoloLens2 activeButton;
    public List<PressableButtonHoloLens2> buttons;
    public string platePath  = "none";
    public Material highlightMaterial;
    public Material normalMaterial;

    public string activeButtonText;

    public ButtonEvent onToggle;

    void start()
    {
        buttons = new List<PressableButtonHoloLens2>();
    }

    public void toggleButton(GameObject source = null)
    {
        var touchedButton = source.GetComponent<PressableButtonHoloLens2>();
        foreach (var button in buttons)
        {            
            if (button == touchedButton)
            {
                if (platePath != "none")
                {
                    button.transform.Find(platePath).gameObject.GetComponent<MeshRenderer>().material = highlightMaterial;
                }
                activeButton = button;
                onToggle.Invoke(this);
            }
            else
            {
                if (platePath != "none")
                {
                    button.transform.Find(platePath).gameObject.GetComponent<MeshRenderer>().material = normalMaterial;
                }
            }         
        }
    }

    public void toggleActiveButton()
    {
        if (activeButton != null)
        {
            foreach (var button in buttons)
            {
                if (button == activeButton )
                {
                    if (platePath != "none")
                    {
                        button.transform.Find(platePath).gameObject.GetComponent<MeshRenderer>().material = highlightMaterial;
                    }
                }
                else
                {
                    if (platePath != "none")
                    {
                        button.transform.Find(platePath).gameObject.GetComponent<MeshRenderer>().material = normalMaterial;
                    }
                }
            }
        }
    }

    public void saveSelectedButtonByText()
    {
        if (activeButton != null)
        {
            activeButtonText = activeButton.gameObject.GetComponent<ButtonConfigHelper>().MainLabelText;
        } 
    }

    public void restoreSelectedButtonByText()
    {
        if (activeButtonText != null)
        {
            foreach (var button in buttons)
            {
                string buttonText = button.gameObject.GetComponent<ButtonConfigHelper>().MainLabelText;
                if (activeButtonText == buttonText)
                {
                    activeButton = button;
                }
            }
        } 
    }
}

[System.Serializable]
public class ButtonEvent : UnityEvent<ToggleButton>
{
}
