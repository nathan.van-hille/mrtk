﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotationByStep : MonoBehaviour
{
    public int verticalAxis, horizontalAxis, depthAxis;
    public Transform graph;


    public void rotateUp()
    {
        var rotation = new Vector3(0f, 0f, 0f);
        rotation[horizontalAxis] += 90;
        graph.Rotate(rotation);
        var temp = verticalAxis;
        verticalAxis = depthAxis;
        depthAxis = temp;
    }
    public void rotateDown()
    {
        var rotation = new Vector3(0f, 0f, 0f);
        rotation[horizontalAxis] -= 90;
        graph.Rotate(rotation);
        var temp = verticalAxis;
        verticalAxis = depthAxis;
        depthAxis = temp;
    }
    public void rotateLeft()
    {
        var rotation = new Vector3(0f, 0f, 0f);
        rotation[verticalAxis] += 90;
        graph.Rotate(rotation);
        var temp = horizontalAxis;
        horizontalAxis = depthAxis;
        depthAxis = temp;
    }
    public void rotateRight()
    {
        var rotation = new Vector3(0f, 0f, 0f);
        rotation[verticalAxis] -= 90;
        graph.Rotate(rotation);
        var temp = horizontalAxis;
        horizontalAxis = depthAxis;
        depthAxis = temp;
    }
    public void rotateCenter()
    {
        var rotation = new Vector3(0f, 0f, 0f);
        rotation[depthAxis] += 90;
        graph.Rotate(rotation);
        var temp = verticalAxis;
        verticalAxis = horizontalAxis;
        horizontalAxis = temp;
    }
}
