﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Microsoft.MixedReality.Toolkit.UI;
using TMPro;

public class TimeFrame : DataObject
{

    public PinchSlider minTimeSlider, maxTimeSlider;
    public TextMeshPro minSliderLabel, maxSliderLabel;
    public Transform trajectoryParent;
    public DataRepository data;

    //public HashSet<int> selectedIds = new HashSet<int>();
    public List<int> selectedIds = new List<int>();

    private float minTime, maxTime;

    public void blockMinSliders()
    {
        if (minTimeSlider.SliderValue > maxTimeSlider.SliderValue)
        {
            minTimeSlider.SliderValue = maxTimeSlider.SliderValue;
        }
    }

    public void blockMaxSliders()
    {
        if (minTimeSlider.SliderValue > maxTimeSlider.SliderValue)
        {
            maxTimeSlider.SliderValue = minTimeSlider.SliderValue;
        }
    }

    public void setMinSliderLabel()
    {
        float minSliderTime = minTimeSlider.SliderValue * (maxTime - minTime) + minTime;
        minSliderLabel.text = minSliderTime.ToString();
    }

    public void setMaxSliderLabel()
    {
        float maxSliderTime = maxTimeSlider.SliderValue * (maxTime - minTime) + minTime;
        maxSliderLabel.text = maxSliderTime.ToString() ;
    }

    public void selectTrajectories()
    {
        selectedIds.Clear();
        var startValues = data.featureList["start"];
        var endValues = data.featureList["stop"];

        maxTime = endValues[0].value ;
        minTime = startValues[0].value; 

        foreach (var pair in startValues)
        {
            if (minTime > pair.value)
            {
                minTime = pair.value;
            }
            if (maxTime < pair.value)
            {
                maxTime = pair.value;
            }
        }
        foreach (var pair in endValues)
        {
            if (minTime > pair.value)
            {
                minTime = pair.value;
            }
            if (maxTime < pair.value)
            {
                maxTime = pair.value;
            }
        }

        float minSliderTime = minTimeSlider.SliderValue * (maxTime - minTime) + minTime;
        float maxSliderTime = maxTimeSlider.SliderValue * (maxTime - minTime) + minTime;

        for (var i =0; i < startValues.Count; i++)
        {
            if( (startValues[i].value >= minSliderTime && startValues[i].value <= maxSliderTime) ||
                (endValues[i].value >= minSliderTime && endValues[i].value <= maxSliderTime) ||
                (startValues[i].value <= minSliderTime && endValues[i].value >= maxSliderTime) )
            {
                selectedIds.Add(data.idList[i]);
            }
            else
            {
                selectedIds.Remove(data.idList[i]);
            }
        }

        foreach(Transform child in trajectoryParent)
        {
            var trajectoryActivation = child.GetComponent<TrajectoryActivation>();
            if (selectedIds.Contains(trajectoryActivation.id)){
                child.gameObject.SetActive(true);
            }
            else
            {
                child.gameObject.SetActive(false);
            }
        }
    }

    public override void launch()
    {
        selectTrajectories();
        setMinSliderLabel();
        setMaxSliderLabel();
    }

}
