﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class AnchorsMovement : MonoBehaviour
{
    private Vector3 frontAnchorNormalized;
    private Vector3 leftAnchorNormalized;
    private Vector3 rightAnchorNormalized;
    private float scale;

    public float boxSize;

    public List<Transform> frontObjects, leftObjects, rightObjects;
    //private List<Vector3> frontObjectsNormalizedPosition, leftObjectsNormalizedPosition, rightObjectsNormalizedPosition;


    public void updatePosition()
    {
        float newScale = transform.localScale.x;
        float newBound = newScale * ((float)Math.Abs(Math.Sin(transform.localEulerAngles.y * (Math.PI / 180))) + (float)Math.Abs(Math.Cos(transform.localEulerAngles.y * (Math.PI / 180)))) * boxSize;
        Vector3 newFrontAnchor = new Vector3(0f, 0f, -newBound);
        Vector3 newLeftAnchor = new Vector3(-newBound, 0f, 0f);
        Vector3 newRightAnchor = new Vector3(newBound, 0f, 0f);

        
        foreach (Transform trans in frontObjects)
        {
            trans.localPosition += (newFrontAnchor - frontAnchorNormalized);
        }
        foreach (Transform trans in leftObjects)
        {
            trans.localPosition += (newLeftAnchor - leftAnchorNormalized) ;
        }
        foreach (Transform trans in rightObjects)
        {
            trans.localPosition += (newRightAnchor - rightAnchorNormalized);
        }
        frontAnchorNormalized = newFrontAnchor;
        leftAnchorNormalized = newLeftAnchor;
        rightAnchorNormalized = newRightAnchor;
        scale = newScale;
    }

    void Start()
    {
        frontAnchorNormalized = new Vector3(0f, 0f, -boxSize);
        leftAnchorNormalized = new Vector3(-boxSize,0f, 0f);
        rightAnchorNormalized = new Vector3(boxSize,0f, 0f);
        scale = 1f;
    }

}
