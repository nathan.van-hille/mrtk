﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Microsoft.MixedReality.Toolkit.UI.BoundsControl;
using Microsoft.MixedReality.Toolkit.UI.BoundsControlTypes;
public class toggleGrpahManipulation : MonoBehaviour
{

    public void toggleManipulation()
    {
        gameObject.GetComponent<BoundsControl>().enabled = !gameObject.GetComponent<BoundsControl>().enabled;
    }

    public void toggleNonUniformManipulation()
    {
        if(gameObject.GetComponent<BoundsControl>().ScaleHandlesConfig.ScaleBehavior != HandleScaleMode.Uniform)
        {
            gameObject.GetComponent<BoundsControl>().ScaleHandlesConfig.ScaleBehavior = HandleScaleMode.Uniform;
        }else
        {
            gameObject.GetComponent<BoundsControl>().ScaleHandlesConfig.ScaleBehavior = HandleScaleMode.NonUniform;
        }
    }
}
