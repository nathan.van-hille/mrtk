﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;

public class TrajectoryActivation : MonoBehaviour
{

    public int id = -1;
    public UnityAction toggle;

    // Start is called before the first frame update
    void Start()
    {
        toggle = new UnityAction(toggleDisplay);
    }
    private void toggleDisplay()
    {
        gameObject.SetActive(true);
    }
    


}
