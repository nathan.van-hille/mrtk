﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrajectoryActivationManager : MonoBehaviour
{

    //ID of the displayed trajectory
    public int idTrajectory = 0;

    //Activates the trajectory with the next id, and disables all the others
    public void enableNextTrajectory()
    {
        if(idTrajectory < gameObject.transform.childCount)
        {
            idTrajectory++;
        }
        else
        {
            idTrajectory = 0;
        }
        var trajectoriesId = gameObject.GetComponentsInChildren<TrajectoryActivation>(true);
        foreach(TrajectoryActivation trajectory in trajectoriesId)
        {
            if(trajectory.id == idTrajectory)
            {
                trajectory.gameObject.SetActive(true);
            }
            else
            {
                trajectory.gameObject.SetActive(false);
            }
        }
    }

    public void enableTrajectory(int id)
    {
        var trajectoriesId = gameObject.GetComponentsInChildren<TrajectoryActivation>(true);
        foreach (TrajectoryActivation trajectory in trajectoriesId)
        {
            if (trajectory.id == id)
            {
                trajectory.gameObject.SetActive(!trajectory.gameObject.activeInHierarchy);
            }
        }
    }
}
