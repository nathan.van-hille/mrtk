﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;
using UnityEngine;
using TMPro;


public class HistogramManager : DataObject
{
    public DataRepository data;
    public TimeFrame time;
    public FilterManager filter;
    public ColorScheemeManager colorSchemeManager;
    public GameObject xAxisLabel, yAxisLabel;
    public GameObject BarParent, BarPrefab;
    public string feature;

    public GameObject LabelPrefab;
    public GameObject tiltePrefab;

    private Dictionary<int,int> entryColorCategory;
    private int[] countPerCategory;
    private int[] selectedPerCategory;
    private int numBar = 2;

    public override void launch()
    {
        entryColorCategory = new Dictionary<int, int>();
        feature = data.columnList[1];
        drawHistogram();
    }

    public void drawHistogram()
    {

        clearHistogram();

        //find colorscheme & set number of bars accordingly
        ColorScheeme colorScheme = colorSchemeManager.getCurrentColorScheme();
        numBar = colorScheme.size();
        countPerCategory = new int[numBar];
        selectedPerCategory = new int[numBar];
        List<idValuePair> featureEntriesList = data.featureList[feature];
        featureEntriesList = featureEntriesList.Where(x => time.selectedIds.Contains(x.id)).ToList();
        var minPair = featureEntriesList[0];
        var maxPair = featureEntriesList[0];
        foreach (var pair in featureEntriesList)
        {
            if (minPair.value > pair.value) { minPair = pair; }
            if (maxPair.value < pair.value) { maxPair = pair; }
        }
        var categoryWitdth = (maxPair.value - minPair.value) / numBar;
        foreach (var pair in featureEntriesList)
        {
            int category = (int)((pair.value - minPair.value) / categoryWitdth);
            if (pair.value == maxPair.value)
            {
                category = numBar - 1;
            }
            entryColorCategory.Add(pair.id, category);
            countPerCategory[category] += 1;
            if (filter.filteredIds.Contains(pair.id))
            {
                selectedPerCategory[category] += 1;
            }
        }


        // Draws the bars
        float barWidth = 5.0f / numBar;
        for (int i = 0; i < numBar; i++)
        {
            GameObject bar_selectedID = Instantiate(BarPrefab, new Vector3(0, 0, 0), Quaternion.identity);
            bar_selectedID.transform.SetParent(BarParent.transform, false);
            bar_selectedID.transform.name = "bar ";

            GameObject bar_notSelectedID = Instantiate(BarPrefab, new Vector3(0, 0, 0), Quaternion.identity);
            bar_notSelectedID.transform.SetParent(BarParent.transform, false);
            bar_notSelectedID.transform.name = "bar ";

            //set correct width & height            
            float x_pos = i * barWidth + barWidth / 2;
            float barHeight = 5.0f * (float)countPerCategory[i] / (float)featureEntriesList.Count;
            float barHeight_selectedID = barHeight * selectedPerCategory[i] /countPerCategory[i];
            float barHeight_notSelectedID = barHeight - barHeight_selectedID;
            bar_selectedID.transform.localPosition = new Vector3(x_pos, barHeight_selectedID / 2, -0.03f);
            bar_selectedID.transform.localScale = new Vector3(barWidth, barHeight_selectedID, bar_selectedID.transform.localScale.z);
            bar_notSelectedID.transform.localPosition = new Vector3(x_pos, barHeight_notSelectedID / 2 + barHeight_selectedID, -0.03f);
            bar_notSelectedID.transform.localScale = new Vector3(barWidth, barHeight_notSelectedID, bar_notSelectedID.transform.localScale.z);

            //set color
            bar_selectedID.GetComponent<Renderer>().material.color = colorScheme.colors[i];
            bar_notSelectedID.GetComponent<Renderer>().material.color = colorScheme.colors[i]/2;

            //add value label
            GameObject label = Instantiate(LabelPrefab, new Vector3(0, 0, 0), Quaternion.identity);
            label.transform.SetParent(BarParent.transform, false);
            label.transform.localPosition = new Vector3(x_pos, barHeight + 0.2f , -0.1f);
            label.GetComponent<TextMeshPro>().text = ""+countPerCategory[i];

        }

        // Draw X Labels
        for (int i = 0; i < numBar + 1; i++)
        {
            GameObject label = Instantiate(LabelPrefab, new Vector3(0, 0, 0), Quaternion.identity);
            label.transform.SetParent(xAxisLabel.transform, false);
            label.transform.localPosition = new Vector3(barWidth * i - 2.5f, -0.3f, 0f);
            label.GetComponent<TextMeshPro>().text = String.Format("{0:G3}" ,(minPair.value + categoryWitdth * i));
        }
        GameObject Titlelabel = Instantiate(tiltePrefab, new Vector3(0, 0, 0), Quaternion.identity);
        Titlelabel.transform.SetParent(xAxisLabel.transform, false);
        Titlelabel.transform.localPosition = new Vector3(5f, 0f, 0f);
        TextMeshPro textmesh = Titlelabel.GetComponent<TextMeshPro>();
        textmesh.text = feature;
        textmesh.alignment = TextAlignmentOptions.TopLeft;

    }

    public void drawHistogram(ToggleButton toggledFeature)
    {
        toggledFeature.saveSelectedButtonByText();
        if (toggledFeature.activeButtonText != null)
        {
            feature = toggledFeature.activeButtonText;
            drawHistogram();
        }
    }

    private void clearHistogram()
    {
        entryColorCategory.Clear();
        countPerCategory = null;
        selectedPerCategory = null;
        numBar = 0;
        //clear bars & bar valu labels
        foreach (Transform bar in BarParent.transform)
        {
            GameObject.Destroy(bar.gameObject);
        }
        foreach( Transform label in xAxisLabel.transform)
        {   
            GameObject.Destroy(label.gameObject);
        }
    }

    public int getCategory(int id)
    {
        return entryColorCategory[id];
    }
    void update() { }
}

