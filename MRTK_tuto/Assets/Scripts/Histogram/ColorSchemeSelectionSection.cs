﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Microsoft.MixedReality.Toolkit.UI;
using Microsoft.MixedReality.Toolkit.Utilities;

using TMPro;

public class ColorSchemeSelectionSection : MonoBehaviour
{
    public GameObject buttonPrefab;

    public ToggleButton toggle;

    public GridObjectCollection buttonParent;
    public TextMeshPro label;

    private float labelHeight = 0.02f;

    public float populateSection(int colorschemeSize, List<ColorScheeme> colorSchemes)
    {
        label.SetText("{0} categories", colorschemeSize);

        foreach (var colorScheme in colorSchemes)
        {
            GameObject button = Instantiate(buttonPrefab,
               new Vector3(0, 0, 0),
               Quaternion.identity);

            button.transform.SetParent(buttonParent.transform, false);
            button.transform.name = "FeatureButton_" + colorScheme.name;

            button.gameObject.GetComponent<ColorSchemeSelectionButtonIcon>().createIcon(colorScheme, colorschemeSize);

            var buttonConfig = button.GetComponent<ButtonConfigHelper>();
            buttonConfig.MainLabelText = colorScheme.name;
            buttonConfig.OnClick.AddListener(() => toggle.toggleButton(button.gameObject));
            toggle.buttons.Add(button.GetComponent<PressableButtonHoloLens2>());
        }
        buttonParent.UpdateCollection();
        return buttonParent.Height + labelHeight;
    }
}
