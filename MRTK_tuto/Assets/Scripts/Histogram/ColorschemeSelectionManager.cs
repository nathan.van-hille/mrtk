﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Microsoft.MixedReality.Toolkit.UI;
using Microsoft.MixedReality.Toolkit.Utilities;
using System.Linq;


public class ColorschemeSelectionManager : MonoBehaviour
{
    public ColorScheemeManager colorschemeData;
    public GameObject buttonPrefab;
    public GameObject sectionPrefab;
    public GameObject sectionParent;
    public List<GameObject> buttonParents;
    public List<GameObject> sectionLabel;

    public ToggleButton toggle;

    private string activeButtonBuffer;

    public void openMenu()
    {
        Clear();
        Populate();
    }

    public void Clear()
    {
        
        foreach (Transform child in sectionParent.transform)
        {
            child.gameObject.SetActive(false);
            GameObject.Destroy(child.gameObject);
        }
        toggle.buttons.Clear();
    }
    public void Populate()
    {
        var colorSchemes = colorschemeData.colorSchemes;
        Dictionary<int, List<ColorScheeme>> colorscheemeSections = new Dictionary<int, List<ColorScheeme>>();
        foreach (var colorScheme in colorSchemes)
        {
            if (colorscheemeSections.ContainsKey(colorScheme.size()))
            {
                colorscheemeSections[colorScheme.size()].Add(colorScheme);
            }
            else
            {
                colorscheemeSections.Add(colorScheme.size(), new List<ColorScheeme>());
                colorscheemeSections[colorScheme.size()].Add(colorScheme);
            }
        }
        var colorschemeSizes = colorscheemeSections.Keys.OrderBy(key=>key);
        var sectionPos = new Vector3(0f, 0f, 0f);
        foreach(var key in colorschemeSizes)
        {
            GameObject section = Instantiate(sectionPrefab,
                new Vector3(0f, 0f, 0f),
                Quaternion.identity);
            section.transform.SetParent(sectionParent.transform, false);

            var sectionManager = section.GetComponent<ColorSchemeSelectionSection>();
            sectionManager.toggle = toggle;
            var sectionHeight = sectionManager.populateSection(key, colorscheemeSections[key]);
            section.transform.localPosition = sectionPos;
            sectionPos.y -= sectionHeight;
        }
            /*
            var grid = buttonParent.GetComponent<GridObjectCollection>();
            var colorSchemes = colorschemeData.colorSchemes;
            foreach (var colorScheme in colorSchemes)
            {
                GameObject button = Instantiate(buttonPrefab,
                new Vector3(0, 0, 0),
                Quaternion.identity);

                button.transform.SetParent(buttonParent.transform, false);
                button.transform.name = "FeatureButton_" + colorScheme.name;

                var buttonConfig = button.GetComponent<ButtonConfigHelper>();
                buttonConfig.MainLabelText = colorScheme.name;
                buttonConfig.OnClick.AddListener(() => toggle.toggleButton(button.gameObject));
                toggle.buttons.Add(button.GetComponent<PressableButtonHoloLens2>());
            }
            grid.UpdateCollection();
            */
    }
}
