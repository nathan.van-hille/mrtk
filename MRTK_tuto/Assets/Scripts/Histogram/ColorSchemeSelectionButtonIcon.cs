﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorSchemeSelectionButtonIcon : MonoBehaviour
{
    public GameObject iconParent;
    public GameObject barPrefab;

    private float boxHeight = 0.020f;
    private float boxWidht = 0.080f;
    private float boxMargin = 0.005f;

    public void createIcon(ColorScheeme colorScheeme, int colorSchemeSize)
    {
        float barWidth = boxWidht / colorSchemeSize;
        for (int i = 0; i<colorSchemeSize; i++)
        {
            GameObject bar = Instantiate(barPrefab, new Vector3(0, 0, 0), Quaternion.identity);
            bar.transform.SetParent(iconParent.transform, false);
            bar.transform.name = "colorBar";

            float x_pos = i * barWidth + barWidth / 2 + boxMargin;
            bar.transform.localPosition = new Vector3(x_pos, 0f, 0f);
            bar.transform.localScale = new Vector3(barWidth, boxHeight, bar.transform.localScale.z);

            bar.GetComponent<Renderer>().material.color = colorScheeme.colors[i];
        }
    }
}
