﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Linq;

public class ConfigLoader : DataInitializer
{
	static string LINE_SPLIT_RE = @"\r\n|\n\r|\n|\r";

	public string configFile;

	public string pointfile;
	public string trackfile;
	public int columnX, columnY, columnZ, columnID;
	public int imagePixelSizeX, imagePixelSizeY, imagePixelSizeZ;
	public float pixelSizeX, pixelSizeY, pixelSizeZ;
	public bool normalizedMesh;

	public float realWorldSize;
	public mainAxis axis;

	public override int initialize()
	{

		Debug.Log("Reading file " + configFile);

		TextAsset data = Resources.Load(configFile) as TextAsset;

		var lines = Regex.Split(data.text, LINE_SPLIT_RE);
		int readline = 0;

		for (var i = 1; i < lines.Length; i++)
		{
			var values = lines[i].Split(' ');
			if (values.Length >= 2)
			{
				switch (values[0])
				{
					case "PointFile":
						pointfile = values[1];
						readline++;
						break;
					case "TrackFile":
						trackfile = values[1];
						readline++;
						break;
					case "ColumnXYZ":
						columnX = System.Convert.ToInt32(values[1], CultureInfo.InvariantCulture);
						columnY = System.Convert.ToInt32(values[2], CultureInfo.InvariantCulture);
						columnZ = System.Convert.ToInt32(values[3], CultureInfo.InvariantCulture);
						readline++;
						break;
					case "ColumnId":
						columnID = System.Convert.ToInt32(values[1], CultureInfo.InvariantCulture);
						readline++;
						break;
					case "ImagePixelSizeXYZ":
						imagePixelSizeX = System.Convert.ToInt32(values[1], CultureInfo.InvariantCulture);
						imagePixelSizeY = System.Convert.ToInt32(values[2], CultureInfo.InvariantCulture);
						imagePixelSizeZ = System.Convert.ToInt32(values[3], CultureInfo.InvariantCulture);
						readline++;
						break;
					case "PixelSizeXYZ":
						pixelSizeX = System.Convert.ToSingle(values[1], CultureInfo.InvariantCulture);
						pixelSizeY = System.Convert.ToSingle(values[2], CultureInfo.InvariantCulture);
						pixelSizeZ = System.Convert.ToSingle(values[3], CultureInfo.InvariantCulture);
						readline++;
						break;
					case "NormalizedMesh":
						normalizedMesh = values[1].Trim(' ','\r','\n') == "true";
						readline++;
						break;
				}
			}
			if(imagePixelSizeX!=0 && imagePixelSizeY != 0 && imagePixelSizeZ !=0 && pixelSizeX!=0 && pixelSizeY!=0 && pixelSizeZ != 0)
            {
				realWorldSize = System.Math.Max(imagePixelSizeX * pixelSizeX, System.Math.Max(imagePixelSizeY * pixelSizeY, imagePixelSizeZ * pixelSizeZ));
				if(imagePixelSizeX* pixelSizeX > imagePixelSizeY * pixelSizeY && imagePixelSizeX * pixelSizeX > imagePixelSizeZ * pixelSizeZ)
                {
					axis = mainAxis.X;
                }
                else
                {
					if(imagePixelSizeY * pixelSizeY > imagePixelSizeZ * pixelSizeZ)
                    {
						axis = mainAxis.Y;
                    }
                    else
                    {
						axis = mainAxis.Z;
                    }
                }
			}
		}
		return readline;
	}
}
