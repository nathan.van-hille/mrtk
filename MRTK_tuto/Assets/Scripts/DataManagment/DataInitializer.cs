﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class DataInitializer : MonoBehaviour
{
    public abstract int initialize();
}
