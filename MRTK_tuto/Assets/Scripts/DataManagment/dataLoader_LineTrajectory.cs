﻿using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System;
using UnityEngine;
using Microsoft.MixedReality.Toolkit.UI;

/*
 * Reads the input file containing points coordinates (x,y,z) and track id for each entry and draws the trajectory.
 */
public class dataLoader_LineTrajectory : DataObject
{
    public ConfigLoader config;

    
    private string inputFile;
    private List<Dictionary<string, object>> pointList;

    public int columnX = 1;
    public int columnY = 2;
    public int columnZ = 3;
    public int columnID = 0;

    // Full column names
    private string xName;
    private string yName;
    private string zName;
    private string idName;

    private float imageRealWorldSize;
    //Line object for one trajectory
    public GameObject line;

    public ColorScheemeManager colorScheemeManager;
    public HistogramManager histogram;
    public FilterManager filter;
    public TimeFrame time; 

    //Parent object
    public GameObject parent;

    //used colorscheeme
    private ColorScheeme coloreScheeme;

    //all generated tracks
    private List<GameObject> generatedTrajectories;

    // Start is called before the first frame update
    public override void launch()
    {
        inputFile = config.pointfile;
        imageRealWorldSize = config.realWorldSize;
        readData();
        drawTracks();
    }
    public void readData()
    {
        pointList = dataReader.Read(inputFile);
        List<string> columnList = new List<string>(pointList[0].Keys);
        generatedTrajectories = new List<GameObject>();

        // Assign column name from columnList to Name variables
        xName = columnList[columnX];
        yName = columnList[columnY];
        zName = columnList[columnZ];
        idName = columnList[columnID];
        
    }
    private void drawTracks()
    {
        var trajectories = new Dictionary<int, List<Vector3>>();
        var trajectoriesId = new List<int>();



        //Instanciate points 3D objects for all points in the list
        for (var i = 0; i < pointList.Count; i++)
        {
            /*
            Debug.Log("x: " + pointList[i][xName]);
            Debug.Log("y: " + pointList[i][yName]);
            Debug.Log("z: " + pointList[i][zName]);
            */

            //Find the normalized coordiantes
            float x = System.Convert.ToSingle(pointList[i][xName], CultureInfo.InvariantCulture) / imageRealWorldSize;
            float y = System.Convert.ToSingle(pointList[i][yName], CultureInfo.InvariantCulture) / imageRealWorldSize;
            float z = System.Convert.ToSingle(pointList[i][zName], CultureInfo.InvariantCulture) / imageRealWorldSize;
            var point = new Vector3(x, y, z);

            var id = System.Convert.ToInt32(pointList[i][idName], CultureInfo.InvariantCulture);

            //Separating points in their respective sequence
            if (trajectories.ContainsKey(id))
            {
                var pointList = trajectories[id];
                pointList.Add(point);
            }
            else
            {
                var pointList = new List<Vector3>();
                pointList.Add(point);
                trajectories[id] = pointList;
                trajectoriesId.Add(id);
            }

        }


        //Draw each trajectory as a line
        foreach (int id in trajectoriesId)
        {
            var pointsArray = trajectories[id].ToArray();
            var numPoints = pointsArray.Length;

            GameObject trajectory = Instantiate(line, 
                new Vector3(0,0,0),
                Quaternion.identity);
            trajectory.transform.SetParent( parent.transform, false);
            trajectory.transform.name = "trajectory " + id;

            LineRenderer lineComponent = trajectory.GetComponent<LineRenderer>();
            lineComponent.positionCount = numPoints;
            lineComponent.SetPositions(pointsArray);

            //put id info into the line
            TrajectoryActivation activationComponent = trajectory.GetComponent<TrajectoryActivation>();
            activationComponent.id = id;

            generatedTrajectories.Add(trajectory);
        }
        colorTracks();


    }

    public void colorTracks()
    {
        //get color scheme
        coloreScheeme = colorScheemeManager.getCurrentColorScheme();
        foreach (GameObject trajectory in generatedTrajectories)
        {
            int id = trajectory.GetComponent<TrajectoryActivation>().id;
            if (time.selectedIds.Contains(id))
            {
                LineRenderer lineComponent = trajectory.GetComponent<LineRenderer>();
                var transparency = lineComponent.endColor.a;
                var endColor = coloreScheeme.colors[histogram.getCategory(id)];
                //endColor.a = transparency;
                lineComponent.endColor = endColor;
                var startColor = coloreScheeme.colors[histogram.getCategory(id)];
                //startColor.a = transparency;
                lineComponent.startColor = startColor;
            }
        }
        
    }

    public void updateLineWidth(List<int> filteredIds)
    {
        colorTracks();
        foreach(GameObject track in generatedTrajectories)
        {
            LineRenderer line = track.GetComponent<LineRenderer>();
            TrajectoryActivation activationComponent = track.GetComponent<TrajectoryActivation>();
            if (filteredIds.Contains(activationComponent.id))
            {
                drawThickTrack(line);
            }
            else
            {
                drawThinTrack(line);
            }
        }
    }

    private void drawThickTrack(LineRenderer line)
    {
        
        line.endWidth = 0.0010f;
        line.startWidth = 0.0010f;
        //line.startColor = new Color(line.startColor.r, line.startColor.g, line.startColor.b, 1);
        //line.endColor = new Color(line.endColor.r, line.endColor.g, line.endColor.b, 1);

    }

    private void drawThinTrack(LineRenderer line)
    {
        line.endWidth = 0.0005f;
        line.startWidth = 0.0005f;
        line.startColor = new Color(Color.grey.r, Color.grey.g, Color.grey.b, 0.4f);
        line.endColor = new Color(Color.grey.r, Color.grey.g, Color.grey.b, 0.4f);

    }

    private float FindMaxValue(string columnName)
    {
        //set initial value to first value
        float maxValue = Convert.ToSingle(pointList[0][columnName], CultureInfo.InvariantCulture);

        //Loop through Dictionary, overwrite existing maxValue if new value is larger
        for (var i = 0; i < pointList.Count; i++)
        {
            if (maxValue < Convert.ToSingle(pointList[i][columnName], CultureInfo.InvariantCulture))
                maxValue = Convert.ToSingle(pointList[i][columnName], CultureInfo.InvariantCulture);
        }

        //Spit out the max value
        return maxValue;
    }

    private float FindMinValue(string columnName)
    {
        float minValue = Convert.ToSingle(pointList[0][columnName], CultureInfo.InvariantCulture);

        //Loop through Dictionary, overwrite existing minValue if new value is smaller
        for (var i = 0; i < pointList.Count; i++)
        {
            if (Convert.ToSingle(pointList[i][columnName], CultureInfo.InvariantCulture) < minValue)
                minValue = Convert.ToSingle(pointList[i][columnName], CultureInfo.InvariantCulture);
        }

        return minValue;
    }
}
