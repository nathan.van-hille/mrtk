﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataLoader : MonoBehaviour
{
    //List of file readers
    public List<DataInitializer> dataInitalizers;

    //List of data dependent objects (/!\ for internal dependency in this list put the objects in order of priority)
    public List<DataObject> dataObjects;

    // Start is called before the first frame update
    void Start()
    {
        foreach(var dataInit in dataInitalizers)
        {
            Debug.Log("Read "+dataInit.initialize() + " in " + dataInit);
        }
        foreach(var dataObj in dataObjects)
        {
            dataObj.launch();
        }
    }
}
