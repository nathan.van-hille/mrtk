﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Globalization;

public class DataRepository : DataInitializer
{
    public ConfigLoader config;

    //Dictionary where each feature has its list of values, indexed by the ID of the object mesured
    //[HideInInspector]
    public Dictionary<string,List<idValuePair>> featureList;
    [HideInInspector]
    public List<string> columnList;
    [HideInInspector]
    public List<int> idList;
    private List<Dictionary<string, object>> trackList;


    public override int initialize()
    {
        
        trackList = dataReader.Read(config.trackfile);
        columnList = new List<string>(trackList[0].Keys);
        featureList = data_toFeatures();
        idList = initialize_IdList();
        return trackList.Count;
    }

    private Dictionary<string, List<idValuePair>> data_toFeatures()
    {
        var dict = new Dictionary<string, List<idValuePair>>();

        //Initializing the dictionaries, ignoring column 1 (ID)
        for(var i = 1; i < columnList.Count; i++)
        {
            dict.Add(columnList[i], new List<idValuePair>());
        }
        //Filling the dictionaries with each entry
        foreach (var entry in trackList)
        {
            int id = System.Convert.ToInt32(entry["id"], CultureInfo.InvariantCulture);
            for (var i =1; i<columnList.Count; i++)
            {
                try
                {
                    var value = System.Convert.ToSingle(entry[columnList[i]], CultureInfo.InvariantCulture);
                    dict[columnList[i]].Add(new idValuePair(id, value));
                }
                catch(System.FormatException e)
                {
                    //Debug.Log("could not read " + entry[columnList[i]] + " as float, throwing it out");
                }
                
                
            }
        }

        return dict;
    }
    private List<int> initialize_IdList()
    {
        List<int> idList = new List<int>();
        foreach (var entry in trackList)
        {
            int id = System.Convert.ToInt32(entry["id"], CultureInfo.InvariantCulture);
            idList.Add(id);
        }
        return idList;
    }
};


public class idValuePair
{
    public int id;
    public float value;
    public idValuePair(int ID, float val) { id = ID; value = val; }
};