﻿using System.Collections;
using System.Collections.Generic;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

using UnityEngine;

public class SaveAndLoad : MonoBehaviour
{
    //Objects with data to save / load
    public FilterManager filterParents;
    public HistogramManager histogram;
    public PipelineManager pipeline;
    public dataLoader_LineTrajectory graph3D;
    public ConfigLoader config;

    //Dataset info
    public DataRepository dataset;

    [HideInInspector]
    [SerializeField] 
    private saveData _saveData = new saveData();

    #region LoadingAndSaving

    public void save(string savename)
    {
        _saveData = createSaveData();
        string saveJson = JsonUtility.ToJson(_saveData);
        string[] saves = Directory.GetFiles(Application.persistentDataPath);
        string savePath = Application.persistentDataPath + "\\" + savename + ".json";
        int index = 2;
        Debug.Log(saves[2]);
        while (Array.IndexOf(saves, savePath) != -1)
        {
            savePath = Application.persistentDataPath + "\\" + savename+ index + ".json";
            index++;
        }
        System.IO.File.WriteAllText(savePath, saveJson);
        Debug.Log("saved " + savePath);
    }

    saveData createSaveData()
    {
        saveData saveObject= new saveData();

        //save dataset name
        saveObject.pointfile = config.pointfile;
        saveObject.trackfile = config.trackfile;

        //save histogram state
        histogramData histogramFields;
        histogramFields.feature = histogram.feature;
        histogramFields.colorScheme = histogram.colorSchemeManager.selectedColorScheme;
        saveObject.histo = histogramFields;

        //save filters state
        var filters = filterParents.transform.GetComponentsInChildren<CumulativeDistributionBarManager>();
        foreach(var filter in filters)
        {
            filterData filterObject;
            filterObject.feature = filter.feature;
            filterObject.sliderXValue = filter.xSlider.SliderValue;
            filterObject.sliderYValue = filter.ySlider.SliderValue;
            filterObject.borneInferieur = filter.borneInferieur;
            filterObject.joint = filter.joint;
            saveObject.filters.Add(filterObject);
        }

        //save pipeline state
        pipelineData pipelineObject;
        pipelineObject.numLine = pipeline.numberOfLines;
        pipelineObject.numColumn = pipeline.numberOfColumns;
        saveObject.pipeline = pipelineObject;
        
        return saveObject;
    }

    public void load(string savepath)
    {
        string jsonFile = System.IO.File.ReadAllText(savepath);
        saveData data = JsonUtility.FromJson<saveData>(jsonFile);

        histogram.colorSchemeManager.selectedColorScheme = data.histo.colorScheme;
        histogram.feature = data.histo.feature;
        histogram.drawHistogram();

        var filters = filterParents.transform.GetComponentsInChildren<CumulativeDistributionBarManager>();
        foreach (var filter in filters)
        {
            filterParents.childFilters.Remove(filter);
            filter.gameObject.SetActive(false);
            GameObject.Destroy(filter.gameObject);
        }
        foreach(var filter in data.filters)
        {
            filterParents.addFilter(filter.feature, filter.joint, filter.borneInferieur?FilterType.LowerBound:FilterType.UpperBound, filter.sliderXValue, filter.sliderYValue);
        }
        filterParents.applyAndPrint();

        pipeline.numberOfLines = data.pipeline.numLine;
        pipeline.numberOfColumns = data.pipeline.numColumn;
        pipeline.UpdateCollection();

        graph3D.colorTracks();
    }

    public bool checkDataSet(string savePath)
    {
        string jsonFile = System.IO.File.ReadAllText(savePath);
        saveData data = JsonUtility.FromJson<saveData>(jsonFile);
        return data.pointfile == config.pointfile && data.trackfile == config.trackfile;
    }
    #endregion


}

[Serializable] 
class saveData
{
    public string pointfile;
    public string trackfile;
    public histogramData histo;
    public pipelineData pipeline;
    public List<filterData> filters = new List<filterData>();
}

[Serializable]
struct filterData
{
    public string feature;
    public float sliderXValue;
    public float sliderYValue;
    public bool borneInferieur;
    public JointType joint;
}

[Serializable]
struct histogramData
{
    public string feature;
    public string colorScheme;

}

[Serializable]
struct pipelineData
{
    public int numLine;
    public int numColumn;
}