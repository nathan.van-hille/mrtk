﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text.RegularExpressions;
using System.Linq;

public class ColorScheemeManager : DataInitializer
{
    static string LINE_SPLIT_RE = @"\r\n|\n\r|\n|\r";

    public string colorPath;
    // Start is called before the first frame update
    //[HideInInspector]
    public List<ColorScheeme> colorSchemes;
    public string selectedColorScheme;

    public override int initialize()
    {
        TextAsset[] data = Resources.LoadAll<TextAsset>(colorPath) ;

        colorSchemes = new List<ColorScheeme>();
        foreach(var colorTextAsset in data){
            ColorScheeme newColorScheeme = new ColorScheeme();
            var lines = Regex.Split(colorTextAsset.text, LINE_SPLIT_RE);
            foreach (var line in lines)
            {
                var values = line.Split(' ').Where(s => !string.IsNullOrEmpty(s)).ToArray();
                float r, g, b;
                if (values.Length == 4)
                {
                    if (float.TryParse(values[0], out r)
                        & float.TryParse(values[1], out g)
                        & float.TryParse(values[2], out b))
                    {
                        Color c = new Color(r / 255, g / 255, b / 255, 1);
                        newColorScheeme.colors.Add(c);
                    }
                }
                if (values.Length == 2)
                {
                    if(values[0] == "Name:")
                    {
                        newColorScheeme.name = values[1];
                    } 
                }
            }
            colorSchemes.Add(newColorScheeme);

        }
        return colorSchemes.Count;
    }

    public void setColorScheme(ToggleButton selectedColorschemeButton)
    {
        selectedColorschemeButton.saveSelectedButtonByText();
        selectedColorScheme = selectedColorschemeButton.activeButtonText;
    }

    public ColorScheeme getCurrentColorScheme()
    {
        return colorSchemes.Find(x => x.name.Contains(selectedColorScheme));
    }
}

public class ColorScheeme
{
    public List<Color> colors;
    public string name;

    public ColorScheeme() { colors = new List<Color>(); }

    public int size() { return colors.Count; }

    public override string ToString() {
        string toString = name + ", size:" + size();
        foreach(Color c in colors)
        {
            toString += ", " + c;
        }
        return toString;
    }
}