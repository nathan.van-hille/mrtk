﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class EnableListner : MonoBehaviour
{
    public UnityEvent onEnableListner;

    void OnEnable()
    {
        onEnableListner.Invoke();
    }
}
