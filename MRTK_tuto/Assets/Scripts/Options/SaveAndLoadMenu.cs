﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using UnityEngine;

using Microsoft.MixedReality.Toolkit.UI;
using Microsoft.MixedReality.Toolkit.Utilities;

public class SaveAndLoadMenu : MonoBehaviour
{
    public SaveAndLoad saveAndLoadDataManager;

    //Save/Load menu
    public GameObject buttonParent;
    public GameObject buttonPrefab;

    public void PopulateLoadMenu()
    {
        Clear();
        var grid = buttonParent.GetComponent<GridObjectCollection>();
        var saves = Directory.GetFiles(Application.persistentDataPath);
        foreach (var saveName in saves)
        {
            if (saveAndLoadDataManager.checkDataSet(saveName))
            {
                var relativeSaveName = saveName.Replace(Application.persistentDataPath + "\\", "");

                GameObject button = Instantiate(buttonPrefab,
                new Vector3(0, 0, 0),
                Quaternion.identity);

                button.transform.SetParent(buttonParent.transform, false);
                button.transform.name = "FeatureButton_" + relativeSaveName;

                var buttonConfig = button.GetComponent<ButtonConfigHelper>();
                buttonConfig.MainLabelText = relativeSaveName;
                buttonConfig.OnClick.AddListener(() => saveAndLoadDataManager.load(saveName));
                buttonConfig.OnClick.AddListener(() => Clear());
            }
        }
        grid.UpdateCollection();

    }

    public void creanteNewSave()
    {
        string saveName = saveAndLoadDataManager.dataset.config.trackfile;
        saveName = saveName.Split('/')[saveName.Split('/').Length-1].ToString();
        saveAndLoadDataManager.save(saveName);
    }

    public void Clear()
    {
        foreach (Transform child in buttonParent.transform)
        {
            child.gameObject.SetActive(false);
            GameObject.Destroy(child.gameObject);
        }
    }
}
