﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Microsoft.MixedReality.Toolkit.UI;

public class MeshOpacity : MonoBehaviour
{
    public PinchSlider slider;
    public Renderer meshRenderer;

    void Start()
    {
        slider.SliderValue = meshRenderer.material.color.a;
        slider.OnValueUpdated.AddListener(updateTransparency);
    }

    public void updateTransparency(SliderEventData eventData)
    {
        meshRenderer.material.color = new Color(meshRenderer.material.color.r, meshRenderer.material.color.g, meshRenderer.material.color.b, eventData.NewValue);
    }
}
