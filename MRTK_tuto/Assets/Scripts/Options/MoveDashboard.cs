﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveDashboard : MonoBehaviour
{
    public Vector3 DashboardRelativePosition;
    public Vector3 HandleRelativePosition;
    public GameObject handle;
    public GameObject dashboard;
    public Transform menuPosition;

    public void setInitialPosition()
    {
        DashboardRelativePosition = dashboard.transform.position - handle.transform.position;
        HandleRelativePosition = menuPosition.InverseTransformPoint(handle.transform.position);
    }

    // Update is called once per frame
    void Update()
    {
        dashboard.transform.position = handle.transform.position + DashboardRelativePosition;
        var head = Camera.main.transform;
        var lookAtPosition = 2 * transform.position - head.position;
        lookAtPosition.y = transform.position.y;
        dashboard.transform.LookAt(lookAtPosition, new Vector3(0f,1f,0f));
        dashboard.transform.Rotate(new Vector3(0f, 30f, 0f));
    }

    public void replaceHandle()
    {
        handle.transform.position = menuPosition.TransformPoint(HandleRelativePosition);
    }
}
