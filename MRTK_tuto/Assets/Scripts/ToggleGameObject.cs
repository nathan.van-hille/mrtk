﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ToggleGameObject: MonoBehaviour
{
    public GameObject activeObject;
    public List<GameObject> objectList;

    public UnityEvent onToggle;


    void start()
    {
        objectList = new List<GameObject>();
    }


    public void toggle(GameObject source = null)
    {
        foreach (var obj in objectList)
        {
            if (obj == source)
            {
                activeObject = obj;
                onToggle.Invoke();
            }
        }
    }  
}

[System.Serializable]
public class ObjectToggleEvent : UnityEvent<ToggleGameObject>
{
}
