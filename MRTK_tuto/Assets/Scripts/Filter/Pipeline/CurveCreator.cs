﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(LineRenderer))]
public class CurveCreator : MonoBehaviour
{
    public Vector3 CenterPos;
    public float angle;
    public LineRenderer linerenderer;
    public float vertexCount = 20;

    // Start is called before the first frame update
    void Start()
    {
        var scale = gameObject.transform.lossyScale;
        var pointList = new List<Vector3>();
        float angleRadian = (float)(Math.PI / 180) * angle;
        for (float angleStep = 0; angleStep <= angleRadian; angleStep += angleRadian / vertexCount)
        {
            var point = new Vector3(CenterPos.x+(float)Math.Sin(angleStep) * scale.x, CenterPos.y+(float)Math.Cos(angleStep) * scale.y, CenterPos.z * scale.z);
            pointList.Add(point);
        }

        linerenderer.positionCount = pointList.Count;
        linerenderer.SetPositions(pointList.ToArray());
    }

    public static void AddCurve(Vector3 CenterPos, float angle, float curveStartAngle, float radius, LineRenderer linerenderer, float vertexCount = 20, bool trigonometricOrder = true, bool vertical = true)
    {
        Vector3[] pointArray = new Vector3[linerenderer.positionCount];
        var pointCount = linerenderer.GetPositions(pointArray);
        List<Vector3> pointList = new List<Vector3>(pointArray) ;

        float endAngleRadian = (float)(Math.PI / 180) * (angle + curveStartAngle);
        float startAngleRadian = (float)(Math.PI / 180) * (curveStartAngle);
        if (trigonometricOrder)
        {
            for (float angleStep = startAngleRadian; angleStep <= endAngleRadian; angleStep += (endAngleRadian - startAngleRadian) / vertexCount)
            {
                if (vertical)
                {
                    var point = new Vector3(CenterPos.x + (float)Math.Sin(angleStep) * radius, CenterPos.y + (float)Math.Cos(angleStep) * radius, CenterPos.z);
                    pointList.Add(point);
                }
                else
                {
                    var point = new Vector3(CenterPos.x + (float)Math.Sin(angleStep) * radius, CenterPos.y, CenterPos.z + (float)Math.Cos(angleStep) * radius);
                    pointList.Add(point);
                }
                
            }
        }
        else
        {
            for (float angleStep = endAngleRadian; angleStep >= startAngleRadian; angleStep -= (endAngleRadian - startAngleRadian) / vertexCount)
            {
                if (vertical)
                {
                    var point = new Vector3(CenterPos.x + (float)Math.Sin(angleStep) * radius, CenterPos.y + (float)Math.Cos(angleStep) * radius, CenterPos.z);
                    pointList.Add(point);
                }
                else
                {
                    var point = new Vector3(CenterPos.x + (float)Math.Sin(angleStep) * radius, CenterPos.y , CenterPos.z + (float)Math.Cos(angleStep) * radius);
                    pointList.Add(point);
                }
            }
        }
        

        linerenderer.positionCount = pointList.Count;
        linerenderer.SetPositions(pointList.ToArray());
    }
}

