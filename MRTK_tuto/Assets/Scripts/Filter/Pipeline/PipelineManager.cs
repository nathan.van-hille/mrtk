﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEditor;
using Microsoft.MixedReality.Toolkit.Utilities;
using Microsoft.MixedReality.Toolkit.UI;    

public class PipelineManager : MonoBehaviour
{
    public int numberOfLines = 2;
    public int numberOfColumns = 2;
    public GridObjectCollection gridCollection;
    public GameObject addButton;
    public GameObject expandRightButton, expandDownButton;
    public GameObject filterCreationMenu;


    public GameObject straightArrowPrefab;
    public GameObject curvedArrowPrefab;
    public GameObject UnionSwitchPrefab, IntersectionSwitchPrefab;
    public GameObject dataSetButton;

    public Transform Pointer;

    private float columnAngle=19;

    public void UpdateCollection()
    {
        var fullLines = (Array.FindAll(gridCollection.transform.GetComponentsInChildren<CumulativeDistributionBarManager>(), t => (t.gameObject.activeSelf)).Count()) / numberOfColumns;
        Debug.Log(fullLines);
        if (fullLines == numberOfLines)
        {
            expandDownButton.SetActive(true);
            expandRightButton.SetActive(true);
            addButton.SetActive(false);
        }
        else
        {
            addButton.SetActive(true);
            expandDownButton.SetActive(false);
            expandRightButton.SetActive(false);
        }
        addButton.transform.SetAsLastSibling();
        gridCollection.UpdateCollection();  
        drawPipeline();
    }
    
    public void drawPipeline()
    {
        var filters = gridCollection.gameObject.GetComponentsInChildren<CumulativeDistributionBarManager>();
        int numColumn = gridCollection.Columns;
        if (filters.Length >= 1)
        {
            var existingArrow = filters[0].gameObject.transform.Find("FilterLink/StraightArrow");
            if (existingArrow != null)
            {
                Destroy(existingArrow.gameObject);
            }
            existingArrow = filters[0].gameObject.transform.Find("FilterLink/CurvedArrow");
            if (existingArrow != null)
            {
                Destroy(existingArrow.gameObject);
            }
            existingArrow = filters[0].gameObject.transform.Find("FilterLink/FirstArrow");
            if (existingArrow != null)
            {
                Destroy(existingArrow.gameObject);
            }
            drawFirstArrow(filters[0]);
        }
        for (int i = 1; i<filters.Length; i++)
        {

            //destroy existing links
            var existingArrow = filters[i].gameObject.transform.Find("FilterLink/StraightArrow");
            if (existingArrow != null)
            {
                Destroy(existingArrow.gameObject);
            }
            existingArrow = filters[i].gameObject.transform.Find("FilterLink/CurvedArrow");
            if (existingArrow != null)
            {
                Destroy(existingArrow.gameObject);
            }
            existingArrow = filters[0].gameObject.transform.Find("FilterLink/FirstArrow");
            if (existingArrow != null)
            {
                Destroy(existingArrow.gameObject);
            }

            //draw the appropriate kind of link to the previous filter
            if (i % numColumn == 0)
            {
                drawCurvedArrow(filters[i],numColumn);
            }
            else
            {
                drawStraightArrow(filters[i]);
            }
        }
    }

    public void drawStraightArrow(CumulativeDistributionBarManager filter)
    {
        GameObject arrow = Instantiate(straightArrowPrefab,
                new Vector3(0, 0, 0),
                Quaternion.identity);
        arrow.transform.SetParent(filter.gameObject.transform.Find("FilterLink"), false);
        arrow.transform.name = "StraightArrow";
        arrow.transform.Translate(0f, 0.0775f, 0f);
        arrow.transform.localScale = new Vector3(0.0775f, 0.0775f, 0.0775f);

        //adding buttons for switching join type
        //finding postion of arrow
        LineRenderer arrowRenderer = arrow.GetComponent<LineRenderer>();
        Vector3 arrowStart = arrowRenderer.GetPosition(0);
        Vector3 arrowEnd = arrowRenderer.GetPosition(arrowRenderer.positionCount - 1);
        //Union
        GameObject buttonUnion= Instantiate(UnionSwitchPrefab,
                new Vector3(0, 0, 0),
                Quaternion.identity);
        buttonUnion.transform.SetParent(arrow.transform, false);
        buttonUnion.transform.name = "switchButtonUnion";
        buttonUnion.transform.localPosition =  (arrowEnd + arrowStart) / 2;
        buttonUnion.transform.Translate(new Vector3(0f, 0f, -0.02f));
        buttonUnion.transform.localScale = new Vector3(10f, 10f, 10f);
        filter.unionButton = buttonUnion; 
        //Intersection
        GameObject buttonIntersection = Instantiate(IntersectionSwitchPrefab,
                new Vector3(0, 0, 0),
                Quaternion.identity);
        buttonIntersection.transform.SetParent(arrow.transform, false);
        buttonIntersection.transform.name = "switchbuttonIntersection";
        buttonIntersection.transform.localPosition = (arrowEnd + arrowStart) / 2;
        buttonIntersection.transform.Translate(new Vector3(0f, 0f, -0.02f));
        buttonIntersection.transform.localScale = new Vector3(10f, 10f, 10f);
        filter.intersectionButton = buttonIntersection;
        //Add listner to button
        buttonIntersection.GetComponent<ButtonConfigHelper>().OnClick.AddListener(() => filter.swapJoinType());
        buttonUnion.GetComponent<ButtonConfigHelper>().OnClick.AddListener(() => filter.swapJoinType());
        if (filter.joint == JointType.Union)
        {
            buttonIntersection.SetActive(false);
        }
        else
        {
            buttonUnion.SetActive(false);
        }
    } 

    public void drawCurvedArrow(CumulativeDistributionBarManager filter, int numColumns)
    {

        GameObject arrow = Instantiate(curvedArrowPrefab,
                new Vector3(0, 0, 0),
                Quaternion.identity);
        arrow.transform.SetParent(filter.gameObject.transform.Find("FilterLink"), false);
        arrow.transform.name = "CurvedArrow";
        arrow.transform.localScale = new Vector3(1f, 1f, 1f);

        //draw line
        LineRenderer renderer = arrow.GetComponent<LineRenderer>();
        float XoffsetToGraphMiddle = -filter.gameObject.transform.Find("FilterLink").localPosition.x;
        float YoffsetToGraphMiddle = -filter.gameObject.transform.Find("FilterLink").localPosition.y;
        
        
        
        var horizontalAngle = columnAngle * (numberOfColumns - 1);
        var horizontalAngleOffset = 5;

        var curveHorizontalCenter = filter.transform.InverseTransformPoint(gridCollection.transform.position);
        curveHorizontalCenter.x = curveHorizontalCenter.x + XoffsetToGraphMiddle;
        curveHorizontalCenter.y = 0f;

        Vector3 startPoint = new Vector3(
            gridCollection.Radius * (float)Math.Sin((horizontalAngle) * Math.PI / 180), 
            gridCollection.CellHeight,
            gridCollection.Radius * (float)Math.Cos((horizontalAngle) * Math.PI / 180));
        Vector3 endPoint = new Vector3(XoffsetToGraphMiddle, 2 * YoffsetToGraphMiddle, 0f);
        startPoint = startPoint + curveHorizontalCenter;
        curveHorizontalCenter.y = ((startPoint + endPoint) / 2).y;

        //begining of the arrow
        addToRenderer(startPoint, renderer);
        //middle of the arrow
        CurveCreator.AddCurve(curveHorizontalCenter, horizontalAngle ,0, gridCollection.Radius, renderer, 10 * numberOfColumns, false, false);
        //end of the arrow
        addToRenderer(endPoint, renderer);
        

        //add arrow        
        Vector3[] pointArray = new Vector3[renderer.positionCount];
        var pointCount = renderer.GetPositions(pointArray);
        List<Vector3> pointList = new List<Vector3>(pointArray);
        Vector3 lastPoint = pointList[pointList.Count - 1];
        pointList.Add(new Vector3(lastPoint.x - 0.01f, lastPoint.y + 0.01f, lastPoint.z));
        pointList.Add(new Vector3(lastPoint.x,lastPoint.y,lastPoint.z));
        pointList.Add(new Vector3(lastPoint.x + 0.01f, lastPoint.y + 0.01f, lastPoint.z));
        pointList.Add(new Vector3(lastPoint.x, lastPoint.y, lastPoint.z));
        renderer.positionCount = pointList.Count;
        renderer.SetPositions(pointList.ToArray());

        //adding buttons for switching join type
        //finding postion of arrow
        LineRenderer arrowRenderer = arrow.GetComponent<LineRenderer>();
        Vector3 arrowStart = arrowRenderer.GetPosition(0);
        Vector3 arrowEnd = arrowRenderer.GetPosition(arrowRenderer.positionCount - 1);
        //Union
        GameObject buttonUnion = Instantiate(UnionSwitchPrefab,
                new Vector3(0, 0, 0),
                Quaternion.identity);
        buttonUnion.transform.SetParent(arrow.transform, false);
        buttonUnion.transform.name = "switchbuttonUnion";
        buttonUnion.transform.localPosition = (arrowEnd + arrowStart) / 2;
        buttonUnion.transform.Translate(new Vector3(0f, 0f, -0.02f));
        buttonUnion.transform.localScale = new Vector3(0.9f, 0.9f, 0.9f);
        filter.unionButton = buttonUnion;
        //Intersction
        GameObject buttonIntersection = Instantiate(IntersectionSwitchPrefab,
                new Vector3(0, 0, 0),
                Quaternion.identity);
        buttonIntersection.transform.SetParent(arrow.transform, false);
        buttonIntersection.transform.name = "switchbuttonIntersection";
        buttonIntersection.transform.localPosition = (arrowEnd + arrowStart) / 2;
        buttonIntersection.transform.Translate(new Vector3(0f, 0f, -0.02f));
        buttonIntersection.transform.localScale = new Vector3(0.9f, 0.9f, 0.9f);
        filter.intersectionButton = buttonIntersection;
        //Add listner to button
        buttonIntersection.GetComponent<ButtonConfigHelper>().OnClick.AddListener(() => filter.swapJoinType());
        buttonUnion.GetComponent<ButtonConfigHelper>().OnClick.AddListener(() => filter.swapJoinType());
        //Display correct button
        if (filter.joint == JointType.Union)
        {
            buttonIntersection.SetActive(false);
        }
        else
        {
            buttonUnion.SetActive(false);
        }
    }

    public void drawFirstArrow(CumulativeDistributionBarManager filter)
    {
        GameObject arrow = Instantiate(curvedArrowPrefab,
                new Vector3(0, 0, 0),
                Quaternion.identity);
        arrow.transform.SetParent(filter.gameObject.transform.Find("FilterLink"), false);
        arrow.transform.name = "FirstArrow";
        arrow.transform.localScale = new Vector3(1f, 1f, 1f);

        //draw line
        LineRenderer renderer = arrow.GetComponent<LineRenderer>();
        Vector3[] pointArray;
        int pointCount;
        List<Vector3> pointList;

        // if aligned draw straigt line
        if (filter.gameObject.transform.localPosition.y == dataSetButton.transform.localPosition.y)
        {
            //draw line
            addToRenderer(filter.transform.InverseTransformPoint(dataSetButton.transform.position) - filter.gameObject.transform.Find("FilterLink").localPosition, renderer);
            addToRenderer(new Vector3(0f, -filter.gameObject.transform.Find("FilterLink").localPosition.y, 0f), renderer);
        }
        //else draw arrow going up
        else
        {
            //set first point
            addToRenderer(filter.transform.InverseTransformPoint(dataSetButton.transform.position) - filter.gameObject.transform.Find("FilterLink").localPosition, renderer);

            //Draw curve
            Vector3 endPoint = filter.gameObject.transform.Find("FilterLink").localPosition + filter.gameObject.transform.localPosition;
            Vector3 startPoint = dataSetButton.transform.localPosition;
            var x_gap = - startPoint.x + (endPoint.x);
            var curveCenter = new Vector3(0f, -filter.gameObject.transform.Find("FilterLink").localPosition.y - x_gap, 0f);
            CurveCreator.AddCurve(curveCenter, 90, 270, x_gap, renderer, 20, true);
        }

        //add arrow
        pointArray = new Vector3[renderer.positionCount];
        pointCount = renderer.GetPositions(pointArray);
        pointList = new List<Vector3>(pointArray);
        Vector3 lastPoint = pointList[pointList.Count - 1];
        pointList.Add(new Vector3(lastPoint.x - 0.01f, lastPoint.y + 0.01f, lastPoint.z));
        pointList.Add(new Vector3(lastPoint.x, lastPoint.y, lastPoint.z));
        pointList.Add(new Vector3(lastPoint.x - 0.01f, lastPoint.y - 0.01f, lastPoint.z));
        pointList.Add(new Vector3(lastPoint.x, lastPoint.y, lastPoint.z));
        renderer.positionCount = pointList.Count;
        renderer.SetPositions(pointList.ToArray());

    }
    
    public void expandDown()
    {
        numberOfLines += 1;
        expandDownButton.transform.Translate(0f, -gridCollection.CellHeight/2, 0f);
        UpdateCollection();
    }

    public void expandRight()
    {
        numberOfColumns += 1;
        gridCollection.Columns = numberOfColumns;
        expandDownButton.transform.localEulerAngles = new Vector3(0f,columnAngle*numberOfColumns/2,0f) ;
        expandRightButton.transform.localEulerAngles = new Vector3(0f, columnAngle * numberOfColumns, 90f) ;
        //expandDownButton.transform.localScale = new Vector3(expandDownButton.transform.localScale.x * ((float)numberOfColumns / (float)(numberOfColumns - 1)),1f,1f)    ;
        expandRightButton.transform.localPosition = new Vector3(
            (float)Math.Sin(columnAngle * numberOfColumns * Math.PI / 180) * gridCollection.Radius,
            expandRightButton.transform.localPosition.y,
            (float)Math.Cos(columnAngle * numberOfColumns * Math.PI / 180) * gridCollection.Radius)+ gridCollection.transform.localPosition;
        expandDownButton.transform.localPosition = new Vector3(
            (float)Math.Sin(columnAngle * numberOfColumns * Math.PI / 360) * gridCollection.Radius,
            expandDownButton.transform.localPosition.y,
            (float)Math.Cos(columnAngle * numberOfColumns * Math.PI / 360) * gridCollection.Radius) + gridCollection.transform.localPosition;
        UpdateCollection();
    }

    public void placeFilterCreationMenu()
    {
        filterCreationMenu.transform.position = addButton.transform.position + 0.05f * Vector3.Normalize(gridCollection.transform.localPosition - addButton.transform.localPosition);
        filterCreationMenu.transform.localEulerAngles = addButton.transform.localEulerAngles;
    }

    void Start()
    {
        gridCollection.Columns = numberOfColumns;
        expandRightButton.transform.localPosition = new Vector3(
            (float)Math.Sin(columnAngle * numberOfColumns * Math.PI / 180) * gridCollection.Radius,
            expandRightButton.transform.localPosition.y,
            (float)Math.Cos(columnAngle * numberOfColumns * Math.PI / 180) * gridCollection.Radius) + gridCollection.transform.localPosition;
        expandRightButton.transform.localEulerAngles = new Vector3(0f, columnAngle * numberOfColumns, 90f);

        expandDownButton.transform.localPosition = new Vector3(
            (float)Math.Sin(columnAngle * numberOfColumns * Math.PI / 360) * gridCollection.Radius,
            expandDownButton.transform.localPosition.y,
            (float)Math.Cos(columnAngle * numberOfColumns * Math.PI / 360) * gridCollection.Radius) + gridCollection.transform.localPosition;
        expandDownButton.transform.localEulerAngles = new Vector3(0f, columnAngle * numberOfColumns / 2, 0f);
        //expandRightButton.transform.localScale = new Vector3(expandRightButton.transform.localScale.x * ((float)numberOfLines), 1f, 1f);
    }


    private void addToRenderer(Vector3 Point, LineRenderer renderer)
    {
        Vector3[] pointArray = new Vector3[renderer.positionCount];
        var pointCount = renderer.GetPositions(pointArray);
        List<Vector3> pointList = new List<Vector3>(pointArray);
        pointList.Add(Point);
        renderer.positionCount = pointList.Count;
        renderer.SetPositions(pointList.ToArray());
    }
}



