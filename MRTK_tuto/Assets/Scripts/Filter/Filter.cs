﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public interface Filter
{

    JointType jointType();
    SelectStatus selectStatus();

    List<int> filterIds();


}

public enum SelectStatus
{
    NoSelection,
    NotSelected,
    EndPointSelected,
    ExclusiveSelected
}