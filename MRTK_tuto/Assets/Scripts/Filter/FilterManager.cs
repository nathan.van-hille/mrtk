﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Microsoft.MixedReality.Toolkit.UI;
using Microsoft.MixedReality.Toolkit.Utilities;

public class FilterManager : DataObject, Filter
{
    //Line manager compo,nent in the parrent of all tracks of the 3D representation
    public dataLoader_LineTrajectory TrackManager;

    //Event called whenever the selected Id list is changed, notably whenever the filters are applied
    public idListEvent onFilteredIdsChanged;

    //All filters in the pipeline
    public List<CumulativeDistributionBarManager> childFilters;

    //ID of all selected tracks
    public List<int> filteredIds;

    //Filter selected in the pipeline as endpoint or exclusive filter
    public Filter selectedFilter;

    //Toggle the activation of the pipeline 
    public bool filtersOn = true;
    
    public GameObject filterPrefab;
    public DataRepository data;
    public TimeFrame time;

    //Prefab for a bar in the CDF
    public GameObject BarPrefab;
    
    public PipelineManager grid;
    
    //Toggle the selected filter in the pipeline
    public ToggleGameObject toggleFilterSelection;

    //parent for duplicate filter
    public Transform filterCopyParent;

    #region FILTERMETHODS

    //Applies the filter pipeline to the track data and returns the list of ids of the selected tracks
    public List<int> filterIds()
    {
        if (filtersOn)
        {
            if (childFilters != null)
            {
                if (childFilters.Count != 0)
                {
                    for (int i = 0; i < childFilters.Count; i++)
                    {
                        if (childFilters[i].selectStatus() == SelectStatus.ExclusiveSelected)
                        {
                            return childFilters[i].filterIds();
                        }
                    }
                    List<int> ids = childFilters[0].filterIds();
                    if (childFilters[0].selectStatus() == SelectStatus.EndPointSelected)
                    {
                        return ids;
                    }
                    for (int i = 1; i < childFilters.Count; i++)
                    {
                        ids = StaticFilter.join(ids, childFilters[i].filterIds(), childFilters[i].jointType());
                        if (childFilters[i].selectStatus() == SelectStatus.EndPointSelected)
                        {
                            return ids;
                        }
                    }
                    return ids;
                }
            }
        }
        
        return data.idList;
    }

    public JointType jointType()
    {
        return JointType.Union;
    }

    public SelectStatus selectStatus()
    {
        return SelectStatus.NotSelected;
    }

    #endregion

    //Set the filteredIds to the ids filterd by the pipeline
    public void  setFilteredId()
    {
        filteredIds= filterIds();
        onFilteredIdsChanged.Invoke(filteredIds);
    }

    //set each filter to the corect activation state depending on wether a filter is selected and in what mode (endpoint or exclusive)
    public void setActiveFilters(CumulativeDistributionBarManager selectedFilter)
    {
        if (filtersOn)
        {
            switch (selectedFilter.filterSelectionSatatus)
            {
                case SelectStatus.EndPointSelected:
                    bool beforeEndPoint = true;
                    foreach (CumulativeDistributionBarManager filter in childFilters)
                    {
                        if (filter == selectedFilter)
                        {
                            beforeEndPoint = false;
                        }
                        else
                        {
                            if (beforeEndPoint)
                            {
                                filter.filterSelectionSatatus = SelectStatus.NoSelection;
                            }
                            else
                            {
                                filter.filterSelectionSatatus = SelectStatus.NotSelected;
                            }
                        }
                        filter.updateColor();
                        filter.highlightGraph();
                    }
                    break;
                case SelectStatus.ExclusiveSelected:
                    foreach (CumulativeDistributionBarManager filter in childFilters)
                    {
                        if (filter != selectedFilter)
                        {
                            filter.filterSelectionSatatus = SelectStatus.NotSelected;
                        }
                        filter.updateColor();
                        filter.highlightGraph();
                    }
                    break;
                case SelectStatus.NoSelection:
                    foreach (CumulativeDistributionBarManager filter in childFilters)
                    {
                        filter.filterSelectionSatatus = SelectStatus.NoSelection;
                        filter.updateColor();
                        filter.highlightGraph();
                    }
                    break;
            }
        }
    }


    //Set all filter to active (if activating the pipeline) or inactive (if deactivating the pipeline)
    public void ToggleFiltersOnOff()
    {
        filtersOn = !filtersOn;
        if (!filtersOn)
        {
            foreach(CumulativeDistributionBarManager filter in childFilters)
            {
                filter.filterSelectionSatatus = SelectStatus.NotSelected;
                filter.updateColor();
                filter.highlightGraph();
            }
        }
        else
        {
            foreach (CumulativeDistributionBarManager filter in childFilters)
            {
                filter.filterSelectionSatatus = SelectStatus.NoSelection;
                filter.updateColor();
                filter.highlightGraph();
            }
        }
    }


    public override string ToString()
    {
        string s = "global filter: ";
        foreach(int id in filteredIds)
        {
            s += "-" + id;
        }
        return s;
    }
  
    //Apply filter pipeline and change the 3D representation accordingly
    public void applyAndPrint()
    {
        setFilteredId();
        TrackManager.updateLineWidth(filteredIds);
    }

    //redraws filters
    public void udpdateFilters()
    {
        foreach (var filter in childFilters)
        {
            filter.drawCumulativeDistribution();
            filter.updateColor();
        }
    }

    //Add a filter at the end of the pipeline
    public void addFilter(string featureName = "length", JointType jointType = JointType.Union, FilterType filterType = FilterType.LowerBound, float xSliderValue = -1, float ySliderValue = -1)
    {
        if(featureName == null)
        {
            featureName = "length";
        }
        GameObject filterObject = Instantiate(filterPrefab,
                new Vector3(0, 0, 0),
                Quaternion.identity);
        var filter = filterObject.GetComponent<CumulativeDistributionBarManager>();

        filterObject.transform.SetParent(gameObject.transform, false);
        filterObject.transform.name = "filter " + featureName;
         
        filter.feature = featureName;

        filter.borneInferieur = (filterType== FilterType.LowerBound);
        filter.infButton.SetActive(filterType == FilterType.LowerBound);
        filter.suppButton.SetActive(filterType == FilterType.UpperBound);

        filter.joint = jointType;
        filter.data = data;
        filter.time = time;
        filter.BarParent = filter.transform.Find("FilterChart/GraphBar").gameObject;
        filter.BarPrefab = BarPrefab;

        //add an update to the global filter when slider are used
        filter.xSlider = filter.transform.Find("FilterChart/GraphBox/Xaxis/PinchSliderX").gameObject.GetComponent<PinchSlider>();
        filter.ySlider = filter.transform.Find("FilterChart/GraphBox/Yaxis/PinchSliderY").gameObject.GetComponent<PinchSlider>();
        filter.launch();
        filter.xSlider.OnValueUpdated.AddListener(x => applyAndPrint());
        filter.ySlider.OnValueUpdated.AddListener(x => applyAndPrint());
        if(xSliderValue!=-1 && ySliderValue != -1)
        {
            filter.setXSliderValue(xSliderValue);
            filter.setYSliderValue(ySliderValue);
        }

        //add an update to the global filter when the mode button is used
        filter.transform.Find("FilterChart/GraphBox/XYplane/InfToSuppButton").gameObject.GetComponent<ButtonConfigHelper>().OnClick.AddListener(()=>applyAndPrint());
        filter.transform.Find("FilterChart/GraphBox/XYplane/SuppToInfButton").gameObject.GetComponent<ButtonConfigHelper>().OnClick.AddListener(() => applyAndPrint());

        //add a filter & visual update when the selection button is used
        filter.transform.Find("FilterChart/GraphBox/XYplane/SelectionButton").gameObject.GetComponent<ButtonConfigHelper>().OnClick.AddListener(() => applyAndPrint());
        filter.transform.Find("FilterChart/GraphBox/XYplane/SelectionButton").gameObject.GetComponent<ButtonConfigHelper>().OnClick.AddListener(
            () => toggleFilterSelection.toggle(filterObject));

        //create a duplicate of the filter when the copy button is pressed
        filter.transform.Find("FilterChart/GraphBox/XYplane/DuplicateButton").gameObject.GetComponent<ButtonConfigHelper>().OnClick.AddListener(() => createFilterCopy(filter));

        childFilters.Add(filter);
        toggleFilterSelection.objectList.Add(filterObject);

        grid.UpdateCollection();
    }

    //Create a copy of the filter, with both filter linked o a change in one is also applyied to the other
    public GameObject duplicateFilter(CumulativeDistributionBarManager filter)
    {
        GameObject copy = Instantiate(filter.gameObject, new Vector3(0f, 0f, 0f), Quaternion.identity);
        foreach (Transform child in copy.transform.Find("FilterLink"))
        {
            GameObject.Destroy(child.gameObject);
        }

        CumulativeDistributionBarManager controller = copy.GetComponent<CumulativeDistributionBarManager>();

        //link switch buttons
        controller.infButton = copy.transform.Find("FilterChart/GraphBox/XYplane/InfToSuppButton").gameObject;
        controller.suppButton = copy.transform.Find("FilterChart/GraphBox/XYplane/SuppToInfButton").gameObject;
        
        var copyInfButtonHelper = controller.infButton.GetComponent<ButtonConfigHelper>();
        var originalInfButtonHelper = filter.infButton.GetComponent<ButtonConfigHelper>();
        copyInfButtonHelper.OnClick.AddListener(() => filter.swapFilterType());
        originalInfButtonHelper.OnClick.AddListener(() => controller.swapFilterType());

        var copySuppButtonHelper = controller.suppButton.GetComponent<ButtonConfigHelper>();
        var originalSuppButtonHelper = filter.suppButton.GetComponent<ButtonConfigHelper>();
        copySuppButtonHelper.OnClick.AddListener(() => filter.swapFilterType());
        originalSuppButtonHelper.OnClick.AddListener(() => controller.swapFilterType());

        //link sliders
        controller.xSlider = copy.transform.Find("FilterChart/GraphBox/Xaxis/PinchSliderX").gameObject.GetComponent<PinchSlider>();
        controller.ySlider = copy.transform.Find("FilterChart/GraphBox/Yaxis/PinchSliderY").gameObject.GetComponent<PinchSlider>();

        filter.xSlider.OnInteractionStarted.AddListener(controller.setActiveSlider);
        filter.ySlider.OnInteractionStarted.AddListener(controller.setActiveSlider);

        controller.xSlider.OnInteractionStarted.AddListener(filter.setActiveSlider);
        controller.ySlider.OnInteractionStarted.AddListener(filter.setActiveSlider);

        controller.xSlider.OnValueUpdated.AddListener(filter.setXSliderValue);
        controller.xSlider.OnValueUpdated.AddListener(filter.syncSliderY);
        controller.xSlider.OnValueUpdated.AddListener(filter.updateColorOnXThreshold);

        controller.ySlider.OnValueUpdated.AddListener(filter.setYSliderValue);
        controller.ySlider.OnValueUpdated.AddListener(filter.syncSliderX);
        controller.ySlider.OnValueUpdated.AddListener(filter.updateColorOnYThreshold);

        filter.xSlider.OnValueUpdated.AddListener(controller.setXSliderValue);
        filter.xSlider.OnValueUpdated.AddListener(controller.syncSliderY);
        filter.xSlider.OnValueUpdated.AddListener(controller.updateColorOnXThreshold);

        filter.ySlider.OnValueUpdated.AddListener(controller.setYSliderValue);
        filter.ySlider.OnValueUpdated.AddListener(controller.syncSliderX);
        filter.ySlider.OnValueUpdated.AddListener(controller.updateColorOnYThreshold);

        //change copy button to delete button
        var copyDuplicateButton = copy.transform.Find("FilterChart/GraphBox/XYplane/DuplicateButton").gameObject;
        var copyDuplicateButtonHelper = copyDuplicateButton.GetComponent<ButtonConfigHelper>();
        copyDuplicateButtonHelper.OnClick.AddListener(destroyFilterCopy);

        return copy;
    }


    //creates a copy of the filter and places it in front of the 3D representation
    public void createFilterCopy(CumulativeDistributionBarManager filter)
    {
        destroyFilterCopy();
        var copy = duplicateFilter(filter);
        copy.transform.SetParent(filterCopyParent, false);
        copy.transform.name = "filter " + filter.feature + "Copy";
    }

    //Destroy any filter duplicate in front of the 3D representation
    public void destroyFilterCopy()
    { 
        foreach (Transform child in filterCopyParent)
        {
            GameObject.Destroy(child.gameObject);
        }
    }

    #region UNITYMETHIDS

    public override void launch()
    {
        childFilters = new List<CumulativeDistributionBarManager>();
        filteredIds = data.idList;
        toggleFilterSelection.onToggle.AddListener(() => setActiveFilters(toggleFilterSelection.activeObject.GetComponent<CumulativeDistributionBarManager>()));
    }

    #endregion

}

[System.Serializable]
public class idListEvent : UnityEvent<List<int>> { }
