﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Microsoft.MixedReality.Toolkit.UI;


//Sends a filter creation request to the filter manager of ther pipeline
public class FilterCreation : MonoBehaviour
{
    //Feature selection menu
    public ToggleButton feature;

    //Pipeline join type
    public ToggleButton joint;

    //Filter threshold type
    public ToggleButton filter;

    //Pipeline filter parent
    public FilterManager filterParent;

    //Gameobject parent to the filter creation menus
    public GameObject filterOptionSelection;

    //Highlight and normal material for the backplate of the button
    public Material activeStateMaterial, deactivatedStateMaterial;

    //backplate of the button
    public Renderer plate;

    
    //Find the option selected in the menus and ask the filter manager of the pipeline to create a new filter with these options.
    public void createFilter()
    {
        string featureName = feature.activeButton.gameObject.GetComponent<ButtonConfigHelper>().MainLabelText;
        JointType jointType = joint.activeButton.gameObject.GetComponent<JointOption>().jointType;
        FilterType filterType = filter.activeButton.gameObject.GetComponent<FilterOption>().filterType;
        filterParent.addFilter(featureName, jointType, filterType);
        filterOptionSelection.SetActive(false);
    }

    //switch between highlighted and normal visual aspect of the button
    public void switchActivation()
    {
        if(feature.activeButton == null)
        {
            plate.material = deactivatedStateMaterial;
        }
        else
        {
            Debug.Log("feature" + feature.activeButton);
            plate.material = activeStateMaterial;
        }
    }
}
