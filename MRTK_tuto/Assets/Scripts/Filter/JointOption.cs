﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JointOption : MonoBehaviour
{
    public JointType jointType;
}


public enum JointType
{
    Union,
    Intersection
}