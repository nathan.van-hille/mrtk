﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class StaticFilter : MonoBehaviour, Filter
{
    public static List<int> join(List<int> idList1, List<int> idList2, JointType jointType)
    {
        switch (jointType)
        {
            case JointType.Union:
                return idList1.Union(idList2).ToList();
            case JointType.Intersection:
                return idList1.Intersect(idList2).ToList();
            default:
                return new List<int>();
        }
    }


    public JointType jointType()
    {
        return JointType.Union;
    }
    public SelectStatus selectStatus()
    {
        return SelectStatus.NotSelected;
    }
    public List<int> filterIds()
    {
        return new List<int>();
    }

}
