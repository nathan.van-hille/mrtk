﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Microsoft.MixedReality.Toolkit.UI;
using TMPro;

public class SliderLabel : MonoBehaviour
{

    public PinchSlider pinchSlider;
    public CumulativeDistributionBarManager filter;

    public void updateXValue()
    {
        gameObject.GetComponent<TextMeshPro>().text = (pinchSlider.SliderValue * (filter.maxValue - filter.minValue) + filter.minValue).ToString("0.00"); ;
    }

    public void updateYValue()
    {
        gameObject.GetComponent<TextMeshPro>().text = (pinchSlider.SliderValue * 100).ToString("0.00");
    }
}
