﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Microsoft.MixedReality.Toolkit.UI;
using TMPro;

public class CumulativeDistributionBarManager :  DataObject, Filter
{

    
    public string feature;

    
    public GameObject infButton, suppButton;
    [HideInInspector]
    public GameObject unionButton, intersectionButton;

    public bool borneInferieur = true;
    public JointType joint = JointType.Union;
    public SelectStatus filterSelectionSatatus = SelectStatus.NoSelection;

    private int numBar;

    public DataRepository data;
    public TimeFrame time;
    public GameObject BarParent, BarPrefab;
    public PinchSlider xSlider, ySlider;
    private PinchSlider selectedSlider;

    public TextMeshPro title;
    public TextMeshPro XminLabel;
    public TextMeshPro XmaxLabel;

    public Material planeMaterial, highlightedPlaneMaterial;

    public List<GameObject> barList;
    [HideInInspector]
    public float minValue;
    [HideInInspector]
    public float maxValue;
    private List<idValuePair> featureEntriesList;

    private Color notSelectedColor = Color.red;
    private Color selectedColor = Color.green;



    public override void launch()
    {

        barList = new List<GameObject>();
        xSlider.SliderValue = borneInferieur ? 1 : 0;
        selectedSlider = xSlider;
        featureEntriesList = data.featureList[feature];
        featureEntriesList.Sort((x, y) => x.value.CompareTo(y.value));
        drawCumulativeDistribution();
    }

    public void drawCumulativeDistribution()
    {
        clear();
        List<idValuePair> subList = featureEntriesList.Where(x => time.selectedIds.Contains(x.id)).ToList();
        numBar = subList.Count();
        var minPair = featureEntriesList[0];
        var maxPair = featureEntriesList[0];
        foreach (var pair in featureEntriesList)
        {
            if (minPair.value > pair.value) { minPair = pair; }
            if (maxPair.value < pair.value) { maxPair = pair; }
        }
        var range = maxPair.value - minPair.value;
        float delta = range / featureEntriesList.Count;
        minValue = minPair.value - delta;
        maxValue = maxPair.value + delta;

        title.text = feature;
        XminLabel.text = minValue.ToString("0.00");
        XmaxLabel.text = maxValue.ToString("0.00");


        // Draws the bars
        float graphWidth = 5.0f;
        for (int i = 1; i < numBar; i++)
        {
            GameObject bar = Instantiate(BarPrefab, new Vector3(0f, 0f, -0f), Quaternion.identity);
            bar.transform.SetParent(BarParent.transform, false);
            bar.transform.name = "bar ";

            //set correct width & height
            float x_start = (subList[i-1].value - (minValue));
            float x_end = (subList[i].value - (minValue));
            float x_pos = graphWidth * (x_end + x_start) * 0.5f / (range + 2f * delta);
            float bar_width = graphWidth * (x_end - x_start) / (range + 2f * delta);
            float barHeight = 5.0f * (float) i / numBar;
            bar.transform.localPosition = new Vector3(x_pos, barHeight / 2, -0.03f);
            bar.transform.localScale = new Vector3(bar_width, barHeight, bar.transform.localScale.z);

            barList.Add(bar);
        }
        GameObject lastBar = Instantiate(BarPrefab, new Vector3(0, 0, 0), Quaternion.identity);
        lastBar.transform.SetParent(BarParent.transform, false);
        lastBar.transform.name = "endBar ";

        float x_pos_last = graphWidth * (range + delta * 1.5f) / (range + 2 * delta);
        lastBar.transform.localPosition = new Vector3(x_pos_last, 2.5f, 0.001f);
        lastBar.transform.localScale = new Vector3(graphWidth * delta / (range + 2 * delta), 5.0f, lastBar.transform.localScale.z);
        barList.Add(lastBar);
    }

    private void clear()
    {
        foreach(var bar in barList)
        {
            bar.SetActive(false);
            GameObject.Destroy(bar);
        }
        barList.Clear();
    }

    #region SLIDER FUNCTIONS
    public void syncSliderX(SliderEventData sliderValue)
    {
        if (selectedSlider == sliderValue.Slider)
        {
            var bar = barList.Find(x => x.transform.localScale.y >= 5 * sliderValue.NewValue);
            float position = bar.transform.localPosition.x;
            float width = bar.transform.localScale.x;
            xSlider.SliderValue = (position - width * 0.5f) / 5f;
        }
    }

    public void syncSliderY(SliderEventData sliderValue)
    {
        if (selectedSlider == sliderValue.Slider)
        {
            var bar = barList.Find(x => (x.transform.localPosition.x + x.transform.localScale.x *0.5f) >= 5 * sliderValue.NewValue);
            if(bar == null)
            {
                bar = barList[barList.Count - 1];
            }
            float height = bar.transform.localScale.y;
            ySlider.SliderValue = height / 5f;
        }
    }

    public void updateColorOnXThreshold(SliderEventData sliderValue)
    {
        if (selectedSlider == sliderValue.Slider)
        {
            foreach (GameObject bar in barList)
            {
                float position = bar.transform.localPosition.x;
                float width = bar.transform.localScale.x;
                if ((position - width * 0.5 < 5 * sliderValue.NewValue) == borneInferieur)
                {
                    setColorGrayScaleCond(bar, notSelectedColor);
                }
                else
                {
                    setColorGrayScaleCond(bar, selectedColor);
                }
            }
        }
    }

    public void updateColorOnYThreshold(SliderEventData sliderValue)
    {

        Debug.Log("Ycolor");
        if (selectedSlider == sliderValue.Slider)
        {
            foreach (GameObject bar in barList)
            {
                float height = bar.transform.localScale.y;
                if ((height <= 5 * sliderValue.NewValue) == borneInferieur)
                {
                    setColorGrayScaleCond(bar, notSelectedColor);
                }
                else
                {
                    setColorGrayScaleCond(bar, selectedColor);
                }
            }
        }
    }

    public void setActiveSlider(SliderEventData sliderValue)
    {
        selectedSlider = sliderValue.Slider;
        if(selectedSlider == ySlider)
        {
            Debug.Log("YSlider");
        }
        if(selectedSlider == xSlider)
        {
            Debug.Log("XSlider");
        }
    }

    public void setXSliderValue(SliderEventData sliderValue)
    {
        if (selectedSlider == sliderValue.Slider)
        {
            xSlider.SliderValue = sliderValue.NewValue;
            Debug.Log(sliderValue.NewValue);
        }
    }

    public void setXSliderValue(float sliderValue)
    {
        xSlider.SliderValue = sliderValue;
    }

    public void setYSliderValue(SliderEventData sliderValue)
    {
        if (selectedSlider == sliderValue.Slider)
        {
            ySlider.SliderValue = sliderValue.NewValue;
        }
    }

    public void setYSliderValue(float sliderValue)
    {
        ySlider.SliderValue = sliderValue;
    }

    #endregion

    public void updateColor()
    {
        foreach (GameObject bar in barList)
        {
            float position = bar.transform.localPosition.x;
            float width = bar.transform.localScale.x;
            if ((position - width * 0.5 < 5 * selectedSlider.SliderValue) == borneInferieur)
            {
                setColorGrayScaleCond(bar, notSelectedColor);
            }
            else
            {
                setColorGrayScaleCond(bar, selectedColor);
            }
        }
    }

    public void setColorGrayScaleCond(GameObject bar, Color c)
    {
        if(filterSelectionSatatus == SelectStatus.NotSelected)
        {
            bar.GetComponent<Renderer>().material.color = new Color(c.grayscale, c.grayscale, c.grayscale);
        }
        else
        {
            bar.GetComponent<Renderer>().material.color = c;
        }
    }


    public void highlightGraph()
    {
        switch (filterSelectionSatatus)
        {
            case SelectStatus.EndPointSelected:
            case SelectStatus.ExclusiveSelected:
                transform.Find("FilterChart/GraphBox/XYplane/Plane").gameObject.GetComponent<Renderer>().material = highlightedPlaneMaterial;
                break;
            default:
                transform.Find("FilterChart/GraphBox/XYplane/Plane").gameObject.GetComponent<Renderer>().material = planeMaterial;
                break;
        }
    }
        

    public void swapFilterType()
    {
        borneInferieur = !borneInferieur;
        if (borneInferieur)
        {
            infButton.SetActive(true);
            suppButton.SetActive(false);
        }
        else
        {
            infButton.SetActive(false);
            suppButton.SetActive(true);
        }
        xSlider.SliderValue = xSlider.SliderValue;
    }

    public void swapJoinType()
    {
        switch (joint)
        {
            case JointType.Union:
                joint = JointType.Intersection;
                unionButton.SetActive(false);
                intersectionButton.SetActive(true);
                break;
            case JointType.Intersection:
                joint = JointType.Union;
                unionButton.SetActive(true);
                intersectionButton.SetActive(false);
                break;
        }
        xSlider.SliderValue = xSlider.SliderValue;
    }

    public void swapSelection()
    {
        switch (filterSelectionSatatus)
        {
            case SelectStatus.NoSelection:
                filterSelectionSatatus = SelectStatus.EndPointSelected;
                break;
            case SelectStatus.NotSelected:
                filterSelectionSatatus = SelectStatus.EndPointSelected;
                break;
            case SelectStatus.EndPointSelected:
                filterSelectionSatatus = SelectStatus.ExclusiveSelected;
                break;
            case SelectStatus.ExclusiveSelected:
                filterSelectionSatatus = SelectStatus.NoSelection;
                break;
        }
    }

    public void setSelection(CumulativeDistributionBarManager selectedFilter)
    {
        if (selectedFilter != this)
        {
            if(selectedFilter.filterSelectionSatatus != SelectStatus.NoSelection)
            {
                filterSelectionSatatus = SelectStatus.NotSelected;
                updateColor();
            }
            else
            {
                filterSelectionSatatus = SelectStatus.NoSelection;
                updateColor();
            }
        }
    }

    public Vector3 getWorldSize()
    {
        return gameObject.transform.Find("GraphBox/XYplane/Plane").gameObject.GetComponent<Renderer>().bounds.size;
    }

    

    #region FILTERMETHODS

    public List<int> filterIds()
    {
        List<int> ids = new List<int>();
        foreach(idValuePair pair in featureEntriesList)
        {
            float sliderValue = xSlider.SliderValue * (maxValue - minValue)+minValue ;
            if ( (pair.value>sliderValue) == borneInferieur)
            {
                ids.Add(pair.id);
            }
        }
        return ids;
    }

    public JointType jointType()
    {
        return joint;
    }

    public SelectStatus selectStatus()
    {
        return filterSelectionSatatus;
    }

    #endregion

}


