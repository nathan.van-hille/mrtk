﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Microsoft.MixedReality.Toolkit.UI;

public class FilterOption : MonoBehaviour
{
    public FilterType filterType;
}

public enum FilterType
{
    UpperBound,
    LowerBound
}