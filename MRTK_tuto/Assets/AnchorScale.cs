﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnchorScale : MonoBehaviour
{
    public AnchorsMovement anchorUI;
    // Update is called once per frame
    void Update()
    {
        transform.localPosition = new Vector3(0f, 0.2f, 0f);
        anchorUI.updatePosition();
    }
}
