﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class VolumePlacementManger : DataObject
{
    public ConfigLoader config;
    public GameObject mesh;


    public mainAxis longerAxis;
    public int imagePixelHeight, imagePixelWidth, imageStackCount;
    public float pixelXSize, pixelYSize, slideZHeight;
    public bool normalizedMesh, pixelScaleMesh;

    [HideInInspector]
    public float length, width, depth;

    public void resizeMesh()
    {
        if(pixelScaleMesh && !normalizedMesh)
        {
            length = imagePixelWidth * pixelXSize;
            width = imagePixelHeight * pixelYSize;
            depth = imageStackCount * slideZHeight;

            Vector3 scale = new Vector3(1f, 1f, 1f);
            var maxDim = Math.Max(length, Math.Max(width, depth));
            if (maxDim == length)
            {
                var scaleX = 1f / imagePixelWidth;
                var scaleY = pixelYSize / (imagePixelWidth * pixelXSize);
                var scaleZ = slideZHeight / (imagePixelWidth * pixelXSize);
                scale = new Vector3(scaleX, scaleY, scaleZ);
            }
            else if (maxDim == width)
            {
                var scaleX = pixelXSize / (imagePixelHeight * pixelYSize);
                var scaleY = 1f / imagePixelHeight;
                var scaleZ = slideZHeight / (imagePixelHeight * pixelYSize);
                scale = new Vector3(scaleX, scaleY, scaleZ);
            }
            else if (maxDim == depth)
            {
                var scaleX = pixelXSize / (imageStackCount * slideZHeight);
                var scaleY = pixelYSize / (imageStackCount * slideZHeight);
                var scaleZ = 1f / imageStackCount;
                scale = new Vector3(scaleX, scaleY, scaleZ);
            }

            mesh.transform.localScale = scale;
        }
        if(normalizedMesh && pixelScaleMesh)
        {
            Vector3 newScale;
            switch (longerAxis)
            {
                case mainAxis.X:
                    newScale = new Vector3(1f, (float)pixelYSize / pixelXSize, (float)slideZHeight / pixelXSize);
                    mesh.transform.localScale = newScale;
                    break;
                case mainAxis.Y:
                    newScale = new Vector3((float)pixelXSize / pixelYSize, 1f , (float)slideZHeight / pixelYSize);
                    mesh.transform.localScale = newScale;
                    break;
                case mainAxis.Z:
                    newScale = new Vector3((float)pixelXSize / slideZHeight, (float)pixelYSize / slideZHeight, 1f);
                    mesh.transform.localScale = newScale;
                    break;
            }
        }
    }

    public override void launch()
    {
        longerAxis = config.axis;
        normalizedMesh = config.normalizedMesh;
        imagePixelWidth = config.imagePixelSizeX;
        imagePixelHeight = config.imagePixelSizeY;
        imageStackCount = config.imagePixelSizeZ;
        pixelXSize = config.pixelSizeX;
        pixelYSize = config.pixelSizeY;
        slideZHeight = config.pixelSizeZ;
        resizeMesh();
    }
}

public enum mainAxis
{
    X,
    Y,
    Z
}