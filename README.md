Documentation:
 Tutorial MRTK : https://docs.microsoft.com/en-us/windows/mixed-reality/develop/unity/tutorials/mr-learning-base-01
   Basics of coding with MRTK & Unity
 Begin data visualization : https://sites.psu.edu/bdssblog/2017/04/06/basic-data-visualization-in-unity-scatterplot-creation/ 
   Article about reading data & generating points in the scene acording to the data
 CSV reader : https://bravenewmethod.com/2014/09/13/lightweight-csv-reader-for-unity/
   Code for reading CSV, easily modifable for our .dat format

